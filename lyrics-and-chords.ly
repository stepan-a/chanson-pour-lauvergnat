\version "2.14.2"

\header {
  title = "Chanson pour l‘Auvergnat"
  instrument = "Guitare et chant"
  composer = "Georges Brassens"
  tagline = "stepan@ithaca.fr"
}


<<
    \chords{
      \set chordChanges = ##t
      e2.:m e2.:m b2.:7 b2.:7 b2.:7 b2.:7 e2.:m e2.:m e2.:m e2.:m b2.:7 b2.:7 c2.:6 a2.:m6 b2.:7 b2.:7
      e2.:m e2.:m b2.:7 b2.:7 b2.:7 b2.:7 e2.:m e2.:m e2.:m e2.:m b2.:7 b2.:7 c2.:6 d2.:7 g2. g2.
      e2.:7 a2.:m d2.:7 g2. e2.:m a2.:m b2.:7 e2.:m b2.:7 b2.:7 e2.:m e2.:m c2. a2.:m6 b2.:7 b2.:7
      e2.:m e2.:m b2.:7 b2.:7 b2.:7 b2.:7 e2.:m e2.:m e2.:m e2.:m a2. d2. c4 a4:m b4:7 e2.:m e2.:m
    }

  \relative c' {
    #(set-accidental-style 'default 'Voice)
    \key g \major
    \time 3/4

    e4 e4 fis4 g2 e4 dis2 e4 fis2.
    dis4 dis4 e4 fis2 dis4 e2 fis4 g2.
    e4 e4 fis4 g2 e4 dis2 e4 fis2 g4
    e4 fis4 g4 a4 b4 d4 b2.~ b2.

    e,4 e4 fis4 g2 e4 dis2 e4 fis2.
    dis4 dis4 e4 fis2 dis4 e2 fis4 g2.
    e4 e4 fis4 g2 e4 dis2 e4 fis2 g4
    e4 fis4 g4 a4 b4 c4 d2.~ d2.
    
    b4 c4 d4 e2. a,4 b4 c4 d2. 
    g,4 a4 b4 c2. fis,4 g4 a4 b2.
    dis,4 fis4 a4 c2 a4 g2 e4 g2 b4
    e4 e4 e4 e4 dis4 c4 b2.~ b2.
    
    e,4 e4 fis4 g2 e4 dis2 e4 fis2.
    dis4 dis4 e4 fis2 dis4 e2 fis4 g2.
    e4 e4 fis4 g2~ g4 a4 a4 g4 fis2. 
    g2 fis4 e4 e4 e4~ e2.~ e4 r2 
    \mark \markup { \musicglyph #"scripts.segno" }
    \bar "||"
  }
  
  \addlyrics {
    Elle est à toi cet -- te chan --  son,
    Toi l'Au -- ver -- gnat qui, sans fa -- çon,
    M'as don -- né qua -- tre bouts de bois 
    Quand, dans ma vie, il fai -- sait froid, __
    Toi qui m'as don -- né du feu quand
    Les cro -- quan -- tes et les cro -- quants
    Tous les gens bien in -- ten -- tion -- nés,
    M'a -- vaient fer -- mé la porte au nez... __
    Ce n'é -- tait rien qu'un feu de bois,
    Mais il m'a -- vait chauf -- fé le corps
    Et dans mon âme il brûle en -- cor'
    À la ma -- nièr' d'un feu de joie. __
    Toi l'Au -- ver -- gnat quand tu mour -- ras
    Quand le croqu' mort t'em -- por -- te -- ra
    Qu'il te con -- duise, __ à tra -- vers ciel,
    Au Père é -- ter -- nel. __ __
  }
  
  \addlyrics {
    Elle est à toi cet -- te chan -- son,
    Toi, l'Hô -- tes -- se qui, sans fa -- çon
    M'as don -- né qua -- tre bouts de pain
    Quand, dans ma vie, il fai -- sait faim, __
    Toi qui m'ou -- vrit ta hu -- che quand
    Les cro -- quan -- tes et les cro -- quants
    Tous les gens bien in -- ten -- tion -- nés,
    S'a -- mu -- saient à me voir jeû -- ner... __
    Ce n'é -- tait rien qu'un feu de bois,
    Mais il m'a -- vait chauf -- fé le corps
    Et dans mon âme il brûle en -- cor'
    À la ma -- nièr' d'un grand fes -- tin. __
    Toi, l'Hô -- tes -- se, quand tu mour -- ras
    Quand le croqu' mort t'em -- por -- te -- ra
    Qu'il te con -- duise, __ à tra -- vers ciel,
    Au Père é -- ter -- nel. __ __     
  }

  \addlyrics {
    Elle est à toi cet -- te chan --  son,
    Toi l'É -- tran -- ger qui, sans fa -- çon,
    D'un air mal -- heu -- reux m'as sou -- ri 
    Lors -- que les gen -- dar -- mes m'ont pris, __
    Toi qui n'as pas  ap -- plau dit quand
    Les cro -- quan -- tes et les cro -- quants
    Tous les gens bien in -- ten -- tion -- nés,
    Ri -- aient de me voir a -- me -- né... __
    Ce n'é -- tait rien qu'un feu de miel,
    Mais il m'a -- vait chauf -- fé le corps
    Et dans mon âme il brûle en -- cor'
    À la ma -- nièr' d'un grand so -- leil. __
    Toi l'É -- tran -- ger quand tu mour -- ras
    Quand le croqu' mort t'em -- por -- te -- ra
    Qu'il te con -- duise, __ à tra -- vers ciel,
    Au Père é -- ter -- nel. __ __
  }

>>